import {getPedidos} from './pedidos'
import {getClientes} from './cliente'

if(getPedidos().map((x)=> x.taxa).includes('taxa')){
   
}else{
   location.assign('./txentrega.html')
}

const usuarioNome = document.querySelector('#usuario')
usuarioNome.textContent = getClientes()[0].toUpperCase()

const valor = getPedidos().map((e) => e.subt).reduce((a, b) => {
    return a + b
}, 0)

const descontoTaxa = getPedidos().map((e) => e.subt).reduce((a, b) => {
    return a + b
}, -4)

document.querySelector('.btnFim').addEventListener('click', (e) => {
    e.preventDefault()
    
    let dt = new Date()
    let dia = dt.getDate()
    let mes = dt.getMonth()
    let horas = dt.getHours()
    let minutos = dt.getMinutes()

    
    location.assign(`https://wa.me/558186197535?text=%2ACardapio%20Digital%2A%20%0A%0A%0A%2APedido%20realizado%20em%20${dia}%20%2F%20${mes+1}%20as%20${horas}%3A${minutos}%2A%0A%2ANome%2A%3A%20${getClientes()[0].normalize('NFD').replace(/[\u0300-\u036f]/g, '')}%2C%0A%2ACel%2A%3A%20${getClientes()[4]}%2C%0A%0A%2ACEP%2A%3A%20${getClientes()[3]}%2C%0A%2AEndereco%2A%3A%20${getClientes()[1].normalize('NFD').replace(/[\u0300-\u036f]/g, '')}%2C%20${getClientes()[2]}%2C%0A%2ABairro%2A%3A%20${getPedidos().map((z) => z.nomeDoBairro).join('').normalize('NFD').replace(/[\u0300-\u036f]/g, '')}%2C%0A%2AP.%20de%20Referencia%2A%3A%20${getClientes()[5].normalize('NFD').replace(/[\u0300-\u036f]/g, '')}%2C%0A%0A%2APedido%2A%0A${getPedidos().map((x) => [x.qtd, x.produto.normalize('NFD').replace(/[\u0300-\u036f]/g, ''), 'R$ ' + x.subt.toFixed(2).replace('.', ',')].join(' ')).join(' -%0A')}%0A%0A%2AObservacao%2A%3A%20${getClientes()[8].normalize('NFD').replace(/[\u0300-\u036f]/g, '')}%2C%0A%2AForma%20de%20Pagamento%2A%3A%20${getClientes()[6].normalize('NFD').replace(/[\u0300-\u036f]/g, '')}%2C%0A%2ATroco%2A%3A%20${getClientes()[7].normalize('NFD').replace(/[\u0300-\u036f]/g, '')}%2C%0A%2AValor%20total%2A%3A%20${valor.toFixed(2).replace('.', ',')}%0ASiga-nos%20no%20instagram%20%40crocantecrispys`)
    
    
    
    
})