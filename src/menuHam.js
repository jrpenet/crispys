import { criatemp } from './temp'

//gera o menu na tela 

//cardapio da tela
const cardapio = [{
    nomeHamburguer: 'Coxa da Asa mais Asinha mais Fritas',
    descricao: 'Porção com 6 coxas da asa + 6 asinhas + 350g de batata frita',
    preco: 39.99,
    foto: './images/coxa da asa asinha Crispy.jpeg',
    tipo: 'coxinha'
},{
    nomeHamburguer: 'Coxa da Asa c/ Fritas',
    descricao: 'Porção com 12 unidades + 350g de batata frita',
    preco: 40.99,
    foto: './images/cx+asa.jpg',
    tipo: 'coxinha'
},{
    nomeHamburguer: 'Coxa da Asinha mais Asinha',
    descricao: 'Porção com 12 unidades',
    preco: 29.99,
    foto: './images/coxacomasa.jpg',
    tipo: 'coxinha'
}
// ,{
//     nomeHamburguer: 'Coxa da Asa c/ Macaxeira Crispy',
//     descricao: 'Porção com 12 coxas + 10 rolinhos',
//     preco: 39.99,
//     foto: './images/logo.png',
//     tipo: 'coxinha'
// }
,{
    nomeHamburguer: 'Coxa da Asa Crocante',
    descricao: 'Porção com 12 unidades',
    preco: 29.99,
    foto: './images/cx.jpg',
    tipo: 'coxinha'
},{
    nomeHamburguer: 'Filé de Peixe 10und',
    descricao: '',
    preco: 31.99,
    foto: './images/filedepeixe10.jpg',
    tipo: 'crispy'
},{
    nomeHamburguer: 'Trio Crocante',
    descricao: 'Coxa, Filé de Peixe e Filé de frango. 4 Unidades de cada.',
    preco: 31.99,
    foto: './images/a30cd538-b081-4615-8d57-9dcd0940abdf.jpg',
    tipo: 'crispy'
},{
    nomeHamburguer: 'Medalhão Crispys',
    descricao: 'Acompanha fritas ',
    preco: 32.99,
    foto: './images/medalhao crispys.jpeg',
    tipo: 'crispy'
},{
    nomeHamburguer: 'Filezinho de Peixe c/ Fritas',
    descricao: 'Serve 01 pessoa. Acompanha 02 filés de peixe + batata frita 250g.',
    preco: 22.99,
    foto: './images/filedepeixeoufrango.jpg',
    tipo: 'crispy'
},{
    nomeHamburguer: 'Filezinho de Frango 06 unidades c/ Fritas',
    descricao: '150g de fritas',
    preco: 31.99,
    foto: './images/filepeixefra.jpg',
    tipo: 'crispy'
},{
    nomeHamburguer: 'Filezinho de Peixe 06 unidades c/ Fritas',
    descricao: '150g de fritas',
    preco: 31.99,
    foto: './images/filepeixefra.jpg',
    tipo: 'crispy'
},{
    nomeHamburguer: 'Filezinho de Frango Crocante (com fritas)',
    descricao: 'Acompanha batata frita 300g',
    preco: 39.99,
    foto: './images/file12undcomfritas.jpg',
    tipo: 'crispy'
},
// {
//     nomeHamburguer: 'Filezinho de Frango Crocante c/ Rolinhos de Macaxeira',
//     descricao: 'Acompanha Cheddar',
//     preco: 39.99,
//     foto: './images/file com rolinho.jpeg',
//     tipo: 'crispy'
// },
{
   nomeHamburguer: 'Cheese Crispy\'s',
   descricao: 'Cheese Crispy\'s',
   preco: 27.99,
   foto: './images/logo.png',
   tipo: 'crispy'
},
// {
//     nomeHamburguer: 'Rolinhos de Macaxeira Crispy\'s',
//     descricao: '12 unidades',
//     preco: 21.99,
//     foto: './images/rolinhos.jpeg',
//     tipo: 'crispy'
// },
{
    nomeHamburguer: 'Filezinho de Frango Crocante 6und',
    descricao: '6 unidades',
    preco: 17.99,
    foto: './images/filedefrango6.jpg',
    tipo: 'crispy'
},
{
   nomeHamburguer: 'Crispys de Macaxeira com Cubinhos de Queijo e Cheddar',
   descricao: '',
   preco: 22.99,
   foto: './images/crispymacaxeira.jpeg',
   tipo: 'crispy'
},
{
    nomeHamburguer: 'Crispys de Peixe no Balde',
    descricao: 'Com 15 pedaços de peixe. Acompanha molho.',
    preco: 39.99,
    foto: './images/peixe no balde.jpg',
    tipo: 'crispy'
},
{
    nomeHamburguer: 'Mix Crispy no Balde',
    descricao: '3 Coxas, 03 asas, 03 filé de frango, 03 filezinhos de peixe. Acompanha molho.',
    preco: 39.99,
    foto: './images/logo.png',
    tipo: 'crispy'
},
{
    nomeHamburguer: 'Filezinho de Peixe com Fritas',
    descricao: 'Acompanha porção 300g de batata frita',
    preco: 39.99,
    foto: './images/peixecomfritas.jpg',
    tipo: 'seafood'
},
{
    nomeHamburguer: 'Crispy de Camarão com Fritas',
    descricao: '',
    preco: 39.99,
    foto: './images/crispycamaraocomfritas.jpeg',
    tipo: 'seafood'
},
{
    nomeHamburguer: 'Crispy\'s do Mar',
    descricao: 'Camarão crocante + filezinho de peixe',
    preco: 38.99,
    foto: './images/logo.png',
    tipo: 'seafood'
},{
    nomeHamburguer: 'Crispy\'s do Mar com Fritas',
    descricao: 'Camarão crocante + filezinho de peixe + fritas',
    preco: 40.99,
    foto: './images/crispysdomarcomfritas.jpeg',
    tipo: 'seafood'
}
,{
   nomeHamburguer: 'Cubos de Queijo Crocante',
   descricao: 'Porção com 08 unidades',
   preco: 29.99,
   foto: './images/cubosqjocro.jpeg',
   tipo: 'acompanhamento'
}
// ,{
//     nomeHamburguer: 'Rolinho de Macaxeira Crocante',
//     descricao: 'Peça já!',
//     preco: 21.99,
//     foto: './images/rolinhos.jpeg',
//     tipo: 'acompanhamento'
// }
, {
    nomeHamburguer: 'Batata Frita 350g',
    descricao: 'Porção de 350g',
    preco: 13.99,
    foto: './images/batata neww.jpeg',
    tipo: 'acompanha'
}, {
    nomeHamburguer: 'Batata Frita 400g',
    descricao: 'Porção de 400g',
    preco: 14.99,
    foto: './images/batata neww.jpeg',
    tipo: 'acompanha'
}, 
// {
//     nomeHamburguer: 'Macaxeira com Charque',
//     descricao: 'Porção com 10 unidades de rolinhos de macaxeira',
//     preco: 24.99,
//     foto: './images/crispymacaxeira.jpeg',
//     tipo: 'acompanhamento'
// }, 
// {
//     nomeHamburguer: 'Macaxeira com Calabresa e Catupiry',
//     descricao: 'Porção com 10 unidades de rolinhos de macaxeira',
//     preco: 24.99,
//     foto: './images/crispymacaxeira.jpeg',
//     tipo: 'acompanhamento'
// }, 
{
    nomeHamburguer: 'Rolinhos Primavera Frango com Requeijão',
    descricao: 'Sabor Frango com Requeijão',
    preco: 23.99,
    foto: './images/logo.png',
    tipo: 'acompanhamento'
}, 
{
    nomeHamburguer: 'Aneis de Cebola',
    descricao: '',
    preco: 25.99,
    foto: './images/logo.png',
    tipo: 'acompanhamento'
}
// , {
//     nomeHamburguer: 'Rolinhos Primavera Frango com Legumes',
//     descricao: 'Frango c/ Legumes',
//     preco: 21.99,
//     foto: './images/logo.png',
//     tipo: 'acompanhamento'
//}
// ,{
//     nomeHamburguer: 'Promoção Nuggets mais Fritas mais Molho',
//     descricao: '',
//     preco: 21.99,
//     foto: './images/nuggetsfritasmolho.jpeg',
//     tipo: 'promocoes'
// }
,{
    nomeHamburguer: 'Dupla Crocante',
    descricao: '06 unidades de cada',
    preco: 30.99,
    foto: './images/duplacrocante.jpg',
    tipo: 'promocoes'
 },{
    nomeHamburguer: 'Dose Dupla - Coxinhas',
    descricao: 'Válido apenas nas quintas. Na compra de 6 coxinhas da asinha, você ganha mais 6',
    preco: 22.99,
    foto: './images/2299.jpeg',
    tipo: 'promocoes'
 }, {
   nomeHamburguer: 'Dose Dupla - Asinhas',
   descricao: 'Válido apenas nas quintas. Na compra de 6 asinhas, você ganha mais 6',
   preco: 22.99,
   foto: './images/2299.jpeg',
   tipo: 'promocoes'
}, {
    nomeHamburguer: 'Duelo Crispys',
    descricao: 'Válido apenas nas quintas. Na compra de 5 filezinhos de frango, você ganha mais 5 filezinhos de peixe',
    preco: 22.99,
    foto: './images/duelo22.jpg',
    tipo: 'promocoes'
 }//, {
    // nomeHamburguer: 'Promoção Dupla Fritas',
    // descricao: 'Válido apenas nas sextas. Na compra de uma batata gourmet de 250g, você ganha outra com calabresa e cheddar.',
    // preco: 16.99,
    // foto: './images/2 batatas gourmet.jpeg',
    // tipo: 'promocoes'
//}
, {
    nomeHamburguer: '6 Asinhas',
    descricao: '',
    preco: 17.99,
    foto: './images/asinhas 6und.jpg',
    tipo: 'asinha'
},{
    nomeHamburguer: 'Tulipa da Asa',
    descricao: 'Porção com 10 unidades',
    preco: 29.99,
    foto: './images/tulipa.jpg',
    tipo: 'coxinha'
}, {
    nomeHamburguer: 'Asinha de Frango',
    descricao: 'Porção com 12 unidades',
    preco: 29.99,
    foto: './images/asinha de frango crocante.jpeg',
    tipo: 'asinha'
}, {
    nomeHamburguer: 'Asinha com Fritas',
    descricao: 'Porção com 12 unidades e 350g de batata frita',
    preco: 39.99,
    foto: './images/asinhascomfritas.jpg',
    tipo: 'asinha'
}
// , {
//     nomeHamburguer: 'Asinha com Rolinhos de Macaxeira Crispys',
//     descricao: 'Porção com 12 unidades + 10 rolinhos de macaxeira',
//     preco: 40.99,
//     foto: './images/logo.png',
//     tipo: 'asinha'
// }
, {
    nomeHamburguer: '6 Coxinhas',
    descricao: '',
    preco: 14.99,
    foto: './images/coxa6.jpg',
    tipo: 'coxinha'
}, {
    nomeHamburguer: 'Combo Crispy',
    descricao: 'Crepe nordestino: Massa sírio, pedaços de frango crocante, verduras, milho, catupiry, azeitona, orégano e molho. Grátis 1 refri lata 220ml',
    preco: 22.99,
    foto: './images/combocrispy1.jpeg',
    tipo: 'crepes'
}, {
    nomeHamburguer: 'Combo dos Deuses',
    descricao: 'Massa sírio, banana grelhada, doce de leite, ganache de chocolate e canela. Grátis 1 refri lata 220ml',
    preco: 21.99,
    foto: './images/combocrispy2.jpeg',
    tipo: 'crepes'
}, {
    nomeHamburguer: 'Combo Gaúcho',
    descricao: 'Massa sírio, calabresa, verduras, ervilha, queijo coalho e molho. Grátis 1 refri lata 220ml',
    preco: 22.99,
    foto: './images/combocrispy3.jpeg',
    tipo: 'crepes'
}, {
    nomeHamburguer: 'Burrito: Camarão',
    descricao: '2 Unidades. Escolha: Guacamole, pimenta, Cream Sour, Vinagrete e Pasta de Feijão Mexicano',
    preco: 23.99,
    foto: './images/burritocamarao.jpg',
    tipo: 'burritos'
}, {
    nomeHamburguer: 'Camarão com Cream Cheese com Fritas',
    descricao: 'Camarão crocante com recheio de cream cheese',
    preco: 40.99,
    foto: './images/camaraocomcreamcheese.jpg',
    tipo: 'crispy'
}
// , {
//     nomeHamburguer: 'Camarão com Cream Cheese com Rolinhos de Macaxeira',
//     descricao: 'Camarão crocante com recheio de cream cheese. Acompanha rolinhos de macaxeira',
//     preco: 40.99,
//     foto: './images/camaraocomcreamcheese.jpg',
//     tipo: 'crispy'
// }
, {
    nomeHamburguer: 'Coxas e Tulipas',
    descricao: '05 unidades de cada',
    preco: 29.99,
    foto: './images/coxasxtulipas.jpg',
    tipo: 'crispy'
}
// , {
//     nomeHamburguer: 'Mini Rolinhos de Macaxeira',
//     descricao: 'Porção 300g',
//     preco: 17.99,
//     foto: './images/minirolinhosdemacaxeira.jpg',
//     tipo: 'acompanhamento'
// }
, {
    nomeHamburguer: 'Combo Kids',
    descricao: '200g de fritas e um smoothie de 300ml',
    preco: 23.99,
    foto: './images/a94deac9-65e5-402f-8117-b270116607c9.jpg',
    tipo: 'promocoes'
}
]

//retirados 29.01
// ,{
//     nomeHamburguer: 'Filé de Frango 6und com fritas',
//     descricao: 'Acompanha 150g de batata frita',
//     preco: 22.99,
//     foto: './images/filedefrango2999.jpg',
//     tipo: 'crispy'
// }
// ,{
//     nomeHamburguer: 'Filezinho de Frango c/ Fritas',
//     descricao: 'Serve 01 pessoa. Acompanha 03 filés de frango + batata frita 250g.',
//     preco: 22.99,
//     foto: './images/filedefrangocomfritas.jpg',
//     tipo: 'crispy'
// }

const addElements = (lanche) => {
    const dt = new Date()
    const dia = dt.getDay()

    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('button')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeHamburguer
    titulo.setAttribute('class', 'tituloHamburguer')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer')
    imagem.setAttribute('src', lanche.foto)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    btn.textContent = 'PEDIR'
    btn.setAttribute('class', 'btnPedir')
        
    divMain.appendChild(titulo)
    divMain.appendChild(descrito)
    divMain.appendChild(imagem)
    divMain.appendChild(select)
    select.appendChild(opt)
    select.appendChild(opt2)
    select.appendChild(opt3)
    divMain.appendChild(subValor)
    divMain.appendChild(btn)

    select.addEventListener('change', (e) => {
        const abc = e.target.value * lanche.preco
        subValor.textContent = 'R$ ' + abc.toFixed(2).replace('.', ',')        
    })

    imagem.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeHamburguer
        const price = lanche.preco
        const tp = lanche.tipo
        const img = lanche.foto


        if(dia != 4 && lanche.nomeHamburguer == 'Dose Dupla - Coxinhas' || dia != 4 && lanche.nomeHamburguer == 'Dose Dupla - Asinhas' || dia != 4 && lanche.nomeHamburguer == 'Duelo Crispys'){
            alert('Válido apenas nas Quintas')
        }else if(dia != 5 && lanche.nomeHamburguer == 'Promoção Dupla Fritas' || dia != 5 && lanche.nomeHamburguer == 'Burrito: Camarão'){
            alert('Válido apenas nas Sextas')
        }else{
            criatemp(qt, prodt, price, tp, '', img)
            location.assign('./conftemp.html')
        }
        
    })
    
    
    btn.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeHamburguer
        const price = lanche.preco
        const tp = lanche.tipo
        const img = lanche.foto
        if(dia != 4 && lanche.nomeHamburguer == 'Dose Dupla - Coxinhas' || dia != 4 && lanche.nomeHamburguer == 'Dose Dupla - Asinhas' || dia != 4 && lanche.nomeHamburguer == 'Duelo Crispys'){
            alert('Válido apenas nas Quintas')
        }else if(dia != 5 && lanche.nomeHamburguer == 'Promoção Dupla Fritas' || dia != 5 && lanche.nomeHamburguer == 'Burrito: Camarão'){
            alert('Válido apenas nas Sextas')
        }else{
            criatemp(qt, prodt, price, tp, '', img)
            location.assign('./conftemp.html')
        }
    })

    return divMain
}

const addCardapio = () => {
    cardapio.filter((x) => x.tipo === 'asinha').forEach((lanche) => {
        document.querySelector('#docTable').appendChild(addElements(lanche))
    })
}

const addCardapioTiaGil = () => {
    cardapio.filter((x) => x.tipo === 'coxinha').forEach((lanche) => {
        document.querySelector('#docTable2').appendChild(addElements(lanche))
    })
}

const addCardapioTiaGil2 = () => {
    cardapio.filter((x) => x.tipo === 'promocoes').forEach((lanche) => {
        document.querySelector('#docTable2').appendChild(addElements(lanche))
    })
}

const addCardapioDoce = () => {
    cardapio.filter((x) => x.tipo === 'crispy').forEach((lanche) => {
        document.querySelector('#docTable3').appendChild(addElements(lanche))
    })
}

const addCardapioDoce2 = () => {
    cardapio.filter((x) => x.tipo === 'seafood').forEach((lanche) => {
        document.querySelector('#docTable3').appendChild(addElements(lanche))
    })
}

const addCardapioSalgada = () => {
    cardapio.filter((x) => x.tipo === 'acompanhamento').forEach((lanche) => {
        document.querySelector('#docTable4').appendChild(addElements(lanche))
    })
}

const addCardapioCrepes = () => {
    cardapio.filter((x) => x.tipo === 'crepes').forEach((lanche) => {
        document.querySelector('#docTable5').appendChild(addElements(lanche))
    })
}

const addCardapioBurritos = () => {
    cardapio.filter((x) => x.tipo === 'burritos').forEach((lanche) => {
        document.querySelector('#docTable10').appendChild(addElements(lanche))
    })
}


export {addCardapio, addCardapioBurritos, addCardapioTiaGil, addCardapioTiaGil2, addCardapioDoce, addCardapioDoce2, addCardapioSalgada, addCardapioCrepes, addElements}