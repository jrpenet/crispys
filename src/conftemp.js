import {gettemp} from './temp'
import {criaPedidos} from './pedidos'
// import { addCardapioMolho, addCardapioMolhoEspetinho, addCardapioRecheios } from './molhosrecheios'

if(gettemp().length < 1){
    location.assign('./cardapio.html')
}

//mostra o nome do pedido na pagina
const mostraNomeDoPedido = document.querySelector('#nomePedido')
mostraNomeDoPedido.textContent = gettemp()[0].produto

//mostra a imagem do pedido
const showOrder = document.querySelector('#imagemApr')
showOrder.setAttribute('src', gettemp()[0].foto)

//pega o q foi digitado em obs
const pegaObs = document.querySelector('#insereObs')

//botao que esta na page
const botaoConfirmaInsere = document.querySelector('#confirmaInsere')

if(gettemp()[0].produto === 'Batata Frita Gourmet'){
    const txtmolho = document.querySelector('#textoMolho')
    txtmolho.setAttribute('style', 'display:none;')

    const txtValor = document.querySelector('#textoValor')
    txtValor.setAttribute('style', 'display:none;')

    const lstMolho = document.querySelector('#listaMolho')
    lstMolho.setAttribute('style', 'display:none;')

    const showBatata = document.querySelector('#infoBatata')
    showBatata.removeAttribute('style')
}

if(gettemp()[0].produto === 'Milkshake'){
    const saborMilk = document.querySelector('#secMilk')
    saborMilk.removeAttribute('style', 'display: none;')
}

if(gettemp()[0].tipo === 'suco'){
    const saborJuice = document.querySelector('#secSuco')
    saborJuice.removeAttribute('style', 'display: none;')
}

if(gettemp()[0].tipo === 'burritos'){
    const txtmolho = document.querySelector('#textoMolho')
    txtmolho.setAttribute('style', 'display:none;')

    const txtValor = document.querySelector('#textoValor')
    txtValor.setAttribute('style', 'display:none;')

    const lstMolho = document.querySelector('#listaMolho')
    lstMolho.setAttribute('style', 'display:none;')

    const saborJuice = document.querySelector('#secBurrito')
    saborJuice.removeAttribute('style', 'display: none;')
}

if(gettemp()[0].produto === 'Batata Frita Gourmet - Mista 400g' || gettemp()[0].produto === 'Rolinho de Macaxeira Crocante' || gettemp()[0].tipo === 'milkshake' || gettemp()[0].tipo === 'suco'){
    const txtmolho = document.querySelector('#textoMolho')
    txtmolho.setAttribute('style', 'display:none;')

    const txtValor = document.querySelector('#textoValor')
    txtValor.setAttribute('style', 'display:none;')

    const lstMolho = document.querySelector('#listaMolho')
    lstMolho.setAttribute('style', 'display:none;')
}

if(gettemp()[0].produto === 'Rolinhos Primavera'){
    const saborRolinn = document.querySelector('#secRolinho')
    saborRolinn.removeAttribute('style', 'display: none;')
}

if(gettemp()[0].tipo === 'crepes'){
    const saborLata = document.querySelector('#secLata')
    saborLata.removeAttribute('style', 'display: none;')
}

botaoConfirmaInsere.addEventListener('click', (e) => {
    e.preventDefault()   
    
    let precos = document.querySelectorAll('[preco]')
    
    let priceProd = 0

    for(let i =0; i < precos.length; i++){
        if(precos[i].checked === true){
            priceProd += parseFloat(precos[i].attributes[1].value)
        }
    }
    
    let boxes = document.querySelectorAll('.checado')
    let rs = ''

    for(let i = 0; i < boxes.length; i++){
        if(boxes[i].checked === true){
            rs += boxes[i].value + ", "
        }
    }

    let counter = document.querySelectorAll('input[type=checkbox]:checked').length

    //batata
    
    const porcaoBatata350g = document.querySelector('#portion350g')
    const saborCalab = document.querySelector('#saborCal')

    //milkshake
    const sabormor = document.querySelector('#saborMorango')
    const saboresco = document.querySelector('#saborChoresco') 
    const sabortine = document.querySelector('#saborTine')
    
    //suco
    const saborGoi = document.querySelector('#saborGoiaba')
    const saborCaj = document.querySelector('#saborCaja')
    const saborMarac = document.querySelector('#saborMaracuja')
    const saborManga = document.querySelector('#saborManga')
    const saborTam = document.querySelector('#saborTamarindo')
    const saborA = document.querySelector('#saborAbac')

    //sabor rolinhos primavera
    const saborCatu = document.querySelector('#saborCatupiry')

    //opcao bebida lata
    const saborCoca = document.querySelector('#saborCoca')

    if(gettemp()[0].tipo === 'crepes'){
        if(saborCoca.checked){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Bebida: Coca cola lata Obs: '+ pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo)
        }else{
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Bebida: Fanta lata Obs: '+ pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo)
        }
    }
    else if(gettemp()[0].produto === 'Rolinhos Primavera'){
        if(saborCatu.checked){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Sabor Catupiry Obs: '+ pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo)
        }else{
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Sabor Camarão Obs: '+ pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo)
        }
    }else if(gettemp()[0].tipo === 'suco'){
        if(saborGoi.checked){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Sabor Goiaba Obs: '+ pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo)
        }else if(saborCaj.checked){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Sabor Cajá Obs: '+ pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo)
        }else if(saborMarac.checked){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Sabor Maracujá Obs: '+ pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo)
        }else if(saborManga.checked){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Sabor Manga Obs: '+ pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo)
        }else if(saborTam.checked){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Sabor Tamarindo Obs: '+ pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo)
        }else if(saborA.checked){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Sabor Abacaxi c/ Hortelã Obs: '+ pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo)
        }else{
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Sabor Pitanga Obs: '+ pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo)
        }
    }else if(gettemp()[0].produto === 'Milkshake'){
        if(sabormor.checked){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Sabor Morango Obs: '+ pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo)
        }else if(saboresco.checked){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Sabor Milkchoresco Obs: '+ pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo)
        }else if(sabortine.checked){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Sabor Milkmaltine Obs: '+ pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo)
        }else{
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Sabor Milktropical Obs: '+ pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo)
        }
    }else if(gettemp()[0].produto === 'Batata Frita Gourmet'){
        const txtmolho = document.querySelector('#textoMolho')
        txtmolho.setAttribute('style', 'display:none;')
    
        const txtValor = document.querySelector('#textoValor')
        txtValor.setAttribute('style', 'display:none;')
    
        const lstMolho = document.querySelector('#listaMolho')
        lstMolho.setAttribute('style', 'display:none;')
    
        const showBatata = document.querySelector('#infoBatata')
        showBatata.removeAttribute('style')

        if(porcaoBatata350g.checked){
            if(saborCalab.checked){
                if(rs == ""){
                    criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Calabresa 350g Obs: '+ pegaObs.value, 13.99, gettemp()[0].tipo)
                }else{
                    criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Calabresa 350g Obs: '+ pegaObs.value + ' - Molhos: ' + rs, 13.99 + (gettemp()[0].qtd * priceProd), gettemp()[0].tipo)
                }
            }else{
                if(rs == ""){
                    criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Charque e Cheddar 350g Obs: '+ pegaObs.value, 13.99, gettemp()[0].tipo)
                }else{
                    criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Charque e Cheddar 350g Obs: '+ pegaObs.value + ' - Molhos: ' + rs, 13.99 + (gettemp()[0].qtd * priceProd), gettemp()[0].tipo)
                }
            }
        }else{
            if(saborCalab.checked){
                if(rs == ""){
                    criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Calabresa 400g Obs: '+ pegaObs.value, 15.99, gettemp()[0].tipo)
                }else{
                    criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Calabresa 400g Obs: '+ pegaObs.value + ' - Molhos: ' + rs, 15.99 + (gettemp()[0].qtd * priceProd), gettemp()[0].tipo)
                }
            }else{
                if(rs == ""){
                    criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Charque e Cheddar 400g Obs: '+ pegaObs.value, 15.99, gettemp()[0].tipo)
                }else{
                    criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Charque e Cheddar 400g Obs: '+ pegaObs.value + ' - Molhos: ' + rs, 15.99 + (gettemp()[0].qtd * priceProd), gettemp()[0].tipo)
                }
            }
        }
    }else if(gettemp()[0].tipo === 'burritos'){

        let boxesOne = document.getElementsByName('saborBurritos')
        let rsOne = ''

        for(let i = 0; i < boxesOne.length; i++){
            if(boxesOne[i].checked === true){
                rsOne += boxesOne[i].value + ", "
            }
        }

        criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Embutidos: ' + rsOne + ' Obs: '+ pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo)
       
    }else{
        if(rs == ""){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Obs: '+ pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo)
        
        }else{
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Obs: '+ pegaObs.value + ' - Molhos: ' + rs, gettemp()[0].preco + (gettemp()[0].qtd * priceProd), gettemp()[0].tipo)
        }
    }


    
    
    location.assign('./confirma.html')
    
})

