import {getPedidos} from './pedidos'
import {cartTable, insereCart, rodapeCart} from './cart'
import { apagatemp } from './temp'

//refresh automatico pra evitar bugs

(function() {
    if( window.sessionStorage ) {
 
       if( !sessionStorage.getItem( 'firstLoad' ) ) {
          sessionStorage[ 'firstLoad' ] = true;
          window.location.reload();
 
       } else {
          sessionStorage.removeItem( 'firstLoad' );
       }
    }
 })();

cartTable()
insereCart()
rodapeCart()
apagatemp()

document.querySelector('.btn-comprarMais').addEventListener('click', (e) => {
    e.preventDefault()
    location.assign('./index.html')
})

document.querySelector('#finalizarPedido').addEventListener('click', (e) => {
    e.preventDefault()
    if(getPedidos().map((x)=> x.taxa).includes('taxa')){
        location.assign('./forms.html')
    }else{
        location.assign('./txentrega.html')
    }
})