import {criaPedidos} from './pedidos'
import { criatemp } from './temp'

//tabela do menu

const addItemTbl = function (){
    const tabela = document.createElement('table')
    document.querySelector('#docBebidas').appendChild(tabela)
    
    //header da tabela
    
    const tdQT = document.createElement('th')
    tdQT.textContent = 'QTD'
    document.querySelector('table').appendChild(tdQT)
    
    const tdItem2 = document.createElement('th')
    tdItem2.textContent = 'ITEM'
    document.querySelector('table').appendChild(tdItem2)
    
    const tdPRC = document.createElement('th')
    tdPRC.textContent = 'PREÇO'
    document.querySelector('table').appendChild(tdPRC)
    
    const tdpede = document.createElement('th')
    tdpede.textContent = 'PEDIR'
    document.querySelector('table').appendChild(tdpede)
}

//gera o menu na tabela

const listaDeBebidas = [//{
  // nomeDaBebida: 'Guaraná Antártica 1L',
 //  valor: 7.5,
  // imagem: './images/guarana.png'
//},//{
   //nomeDaBebida: 'Pepsi 1L',
 //  valor: 7.5,
   //imagem: './images/pepsi.png'
//},
//{
 //   nomeDaBebida: 'Suco da polpa',
 //   valor: 6,
 //   imagem: './images/sucofru.jpeg',
 //   tipo: 'suco',
 //   foto: './images/sucosnew.jpeg'
//},
{
    nomeDaBebida: 'Smoothie de Frutas',
    valor: 12,
    imagem: './images/novosmoothie.jpg'
}, 
{
    nomeDaBebida: 'Capuccino Creamy',
    valor: 11,
    imagem: './images/capuccinoice.jpeg'
}
]

const addElementsBebidas = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('button')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeDaBebida
    titulo.setAttribute('class', 'tituloHamburguer')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer descricaoBebida')
    imagem.setAttribute('src', lanche.imagem)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.valor.toFixed(2).replace('.', ',')
    btn.textContent = 'PEDIR'
    btn.setAttribute('class', 'btnPedir')
    
    
    divMain.appendChild(titulo)
    divMain.appendChild(descrito)
    divMain.appendChild(imagem)
    divMain.appendChild(select)
    select.appendChild(opt)
    select.appendChild(opt2)
    select.appendChild(opt3)
    divMain.appendChild(subValor)
    divMain.appendChild(btn)

    select.addEventListener('change', (e) => {
        const abc = e.target.value * lanche.valor
        subValor.textContent = 'R$ ' + abc.toFixed(2).replace('.', ',')
        subValor.setAttribute('style', 'color: yellow;')        
    })
    
    
    btn.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeDaBebida
        const price = lanche.valor
        const tipoEl = lanche.tipo

        if(lanche.tipo === 'milkshake' || lanche.tipo === 'suco'){
            criatemp(qt, prodt, price, tipoEl,'', lanche.foto)
        }else{
            criaPedidos(qt, prodt, price, tipoEl)
        }

        if(lanche.tipo === 'milkshake' || lanche.tipo === 'suco'){
            location.assign('./conftemp.html')
        }else{
            location.assign('./txentrega.html')
        }

    })

    return divMain
}

const addMenuBebidas = () => {
    listaDeBebidas.forEach((bebida) => {
        // const linhaHoriz = document.createElement('hr')
        document.querySelector('#docBebidas').appendChild(addElementsBebidas(bebida))
        // document.querySelector('#docBebidas').appendChild(linhaHoriz)
    })
}

export {addItemTbl, addMenuBebidas, addElementsBebidas}